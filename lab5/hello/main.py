import os
from aiohttp import web

async def hello(request: web.Request):
    name = request.rel_url.query.get("name")
    return web.Response(
        text=f"Hello, {name}!",
    )


app = web.Application()
app.add_routes([web.get("/", hello)])

if __name__ == "__main__":
    port = os.getenv("PORT", "8080")
    web.run_app(app, port=int(port))
