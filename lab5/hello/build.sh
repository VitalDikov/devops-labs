#!/bin/sh

set -e

SYSTEM_IMAGE=hello:system
BUILD_IMAGE=hello:build
APP_IMAGE=hello:app

printf "Building system image...\n"
docker build -t "hello:system" -f dockerfiles/system .

printf "Building build image...\n"
docker build --cache-from "$SYSTEM_IMAGE" -t "hello:build" -f dockerfiles/build .

printf "Building app image...\n"
docker build --cache-from "$BUILD_IMAGE" -t "hello:app" -f dockerfiles/app .

printf "OK"
